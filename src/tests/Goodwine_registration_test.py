from src.pages.RegistrationPage import RegistrationClass
import allure


@allure.feature("Registration test")
@allure.title("Регистрация рандомного пользователя")
def test_goodwine_registration(chromeBrowser):
    goodwine_registration = RegistrationClass(chromeBrowser)
    goodwine_registration.go_to_site()
    goodwine_registration.click_registration_button()
    goodwine_registration.enter_name_and_surname()
    goodwine_registration.select_dob()
    goodwine_registration.enter_random_email()
    goodwine_registration.enter_random_phone()
    goodwine_registration.select_preference()
    goodwine_registration.click_registration_button_for_send_form()
    goodwine_registration.make_screenshot('body')




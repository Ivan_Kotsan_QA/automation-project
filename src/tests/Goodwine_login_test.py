from src.pages.HomePage import LoginHelper
import allure
from src.pages.locators import MainPageLocators


@allure.feature("Login test")
@allure.title("Авторизация пользователя")
def test_goodwine_login(operaBrowser):
    # Присвоїти test_goodwine_login класс LoginHelper який лежить в HomePage і
    # передати йому browser(chromedriver)

    goodwine_main_page = LoginHelper(operaBrowser)
    # Визивати з класа LoginHelper функції які ми писали на HomePage
    goodwine_main_page.go_to_site()
    # with allure.step("Click 'Вход' button"):
    goodwine_main_page.click_login_button()
    # with allure.step("Enter email or phone in login field"):
    goodwine_main_page.enter_email(MainPageLocators.email)
    goodwine_main_page.click_send_email_button()
    goodwine_main_page.enter_password()
    goodwine_main_page.click_send_password_button()
    goodwine_main_page.make_screenshot('body')
    goodwine_main_page.click_logout_button()
    goodwine_main_page.make_screenshot('body')

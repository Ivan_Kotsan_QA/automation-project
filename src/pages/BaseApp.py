from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import time
import allure

email = "ivansemenovichwork@gmail.com"


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://gwn-pwa-dev.perspective.net.ua"

    # Всі функції внизу то функції пошука
    # Пошук 1 елемента
    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
                                                      message=f"Can't find element by locator {locator}")

    # Пошук декілька елементів
    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
                                                      message=f"Can't find elements by locator {locator}")

    def element_is_visible(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.visibility_of_all_elements_located(locator),
                                                      message=f"This element is not displayed {locator}")

    def element_to_be_clickable(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.element_to_be_clickable(locator))


    @allure.step("Сделать скриншот body")
    def make_screenshot(self, locator):
        screenshot = allure.attach(
            self.driver.get_screenshot_as_png(),
            name='screenshot',
            attachment_type=allure.attachment_type.PNG
        )
        return screenshot

    def get_email(self):
        return email

    # Дефолтна функція перехода на мейн сайт
    @allure.story("Open maim page")
    def go_to_site(self):
        time.sleep(5)
        return self.driver.get(self.base_url)

    def close_driver(self):
        self.driver.quit()
        self.driver.close()

from selenium.webdriver.common.by import By


class MainPageLocators:
    # Сюда пишеш локатори по чому треба, для локаторів можна створити окремий клас до прикладу
    # HomePageLocators/CatalogPageLocators В данному класі  пишутсья локатори які лише на мейн пейджі
    email = "ivansemenovichwork@gmail.com"
    LOCATOR_LOGIN_BUTTON = (By.XPATH, "//header/div[2]/div[2]/div[1]/div[3]/span[1]")
    LOCATOR_EMAIL_FIELD = (By.ID, "authEmailPhone")
    LOCATOR_SEND_PHONE = (By.XPATH, "//span[contains(text(),'Отправить')]")
    LOCATOR_PASSWORD_FIELD = (By.XPATH, '//input[@name="password"]')
    LOCATOR_SEND_PASSWORD = (By.XPATH, "//body[1]/div[1]/main[1]/aside[1]/div[2]/div[1]/div[1]/form[1]/div[2]/button["
                                       "1]/span[1]")
    LOCATOR_ACCOUNT_IS_VISIBLE = (By.XPATH, "//span[contains(text(),'Sign Out')]")
    LOCATOR_FOR_SCREENSHOTS = (By.TAG_NAME, "body")
    LOCATOR_LOGOUT_BUTTON = (By.XPATH, "//span[contains(text(),'Sign Out')]")


class RegistrationPageLocators:
    LOCATOR_REGISTRATION_BUTTON = (By.XPATH, "//a[contains(text(),'Регистрация')]")
    LOCATOR_PHONE_FIELD_REGISTRATION = (By.NAME, "phone")
    LOCATOR_NAME_FIELD = (By.NAME, "customer.firstname")
    LOCATOR_SURNAME_FIELD = (By.NAME, "customer.lastname")
    LOCATOR_DAY_DOB = (By.NAME, "customer.dob_day")
    LOCATOR_MONTH_DOB = (By.NAME, "customer.dob_month")
    LOCATOR_YEARS_DOB = (By.NAME, "customer.dob_year")
    LOCATOR_EMAIL_FIELD = (By.NAME, "customer.email")
    LOCATOR_PREFERENCE_SELECTOR = (By.CLASS_NAME, "createAccount-item-Ye7")
    LOCATOR_SEND_FORM_REGISTRATION = (By.XPATH, "//span[contains(text(),'Зарегистрироваться')]")

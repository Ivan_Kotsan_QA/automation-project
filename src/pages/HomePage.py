import time

from src.pages.BaseApp import BasePage  # То є подключення базових настройок які в класі BaseApp
import allure
from src.pages.locators import MainPageLocators


# Клас LoginHelper розширений BasePage шоб визивати в тесті+ метод і оттуда go_to_site find_element find_elements
class LoginHelper(BasePage):
    # Сюда пишеш функції в яких обращаєшся до локаторів з locators.py які потом визиваєш в тесті
    @allure.step("Нажать кнопку 'Вход' на главной странице")
    def click_login_button(self):
        time.sleep(5)
        login_button = self.find_element(MainPageLocators.LOCATOR_LOGIN_BUTTON)
        login_button.click()

    @allure.step("Ввести почту в поле email or phone")
    def enter_email(self, email):
        email_field = self.find_element(MainPageLocators.LOCATOR_EMAIL_FIELD)
        email_field.send_keys(email)

    @allure.step("Нажать кнопку Отправить для отправки почты")
    def click_send_email_button(self):
        send_email_button = self.find_element(MainPageLocators.LOCATOR_SEND_PHONE)
        send_email_button.click()
        time.sleep(5)

    @allure.step("Ввести пароль и нажать кнопку Отправить")
    def enter_password(self):
        password_field = self.find_element(MainPageLocators.LOCATOR_PASSWORD_FIELD)
        password_field.send_keys("446688")
        time.sleep(5)

    @allure.step("Нажать кнопку 'Отправить' пароль")
    def click_send_password_button(self):
        password_send_button = self.find_element(MainPageLocators.LOCATOR_SEND_PASSWORD)
        password_send_button.click()
        time.sleep(5)

    @allure.step("Нажать кнопку Sign out для выхода из аккаунта")
    def click_logout_button(self):
        self.find_element(MainPageLocators.LOCATOR_LOGOUT_BUTTON).click()
        time.sleep(5)

from src.pages.BaseApp import BasePage
import allure
from src.pages.locators import RegistrationPageLocators
from random import randint
import time
from selenium.webdriver.support.ui import Select

import random
import string


def get_random_string(length):
    letters = string.ascii_lowercase + string.digits
    return ''.join(random.choice(letters) for i in range(length)) + "@test.com"


def gen_random_number():
    return '11' + str(random_with_N_digits(7))


def random_with_N_digits(n):
    range_start = 10 ** (n - 1)
    range_end = (10 ** n) - 1
    return randint(range_start, range_end)


class RegistrationClass(BasePage):

    @allure.step("Нажать кнопку регистрация на главной странице")
    def click_registration_button(self):
        time.sleep(5)
        self.find_element(RegistrationPageLocators.LOCATOR_REGISTRATION_BUTTON).click()
        time.sleep(5)

    @allure.step("Ввод имени и фамилии")
    def enter_name_and_surname(self):
        self.find_element(RegistrationPageLocators.LOCATOR_NAME_FIELD).send_keys("Automation")
        self.find_element(RegistrationPageLocators.LOCATOR_SURNAME_FIELD).send_keys("Test")

    @allure.step("Выбрать день месяц год")
    def select_dob(self):
        select_date = Select(self.find_element(RegistrationPageLocators.LOCATOR_DAY_DOB))
        select_date.select_by_value("5")
        time.sleep(1)
        select_month = Select(self.find_element(RegistrationPageLocators.LOCATOR_MONTH_DOB))
        select_month.select_by_value("07")
        time.sleep(1)
        select_year = Select(self.find_element(RegistrationPageLocators.LOCATOR_YEARS_DOB))
        select_year.select_by_value("1997")
        time.sleep(1)

    @allure.step("Ввести рандомный номер телефона ")
    def enter_random_phone(self):
        phone_field = self.find_element(RegistrationPageLocators.LOCATOR_PHONE_FIELD_REGISTRATION)
        phone_field.send_keys(gen_random_number())
        time.sleep(1)

    @allure.step("Ввести рандомный email")
    def enter_random_email(self):
        email_field = self.find_element(RegistrationPageLocators.LOCATOR_EMAIL_FIELD)
        email_field.send_keys(get_random_string(15))

    @allure.step("Выбрать все предпочтения на из селекторов")
    def select_preference(self):
        preference = self.find_elements(RegistrationPageLocators.LOCATOR_PREFERENCE_SELECTOR)
        for element in preference:
            element.click()
        time.sleep(2)

    @allure.step("Нажать Регистрация для отправки формы")
    def click_registration_button_for_send_form(self):
        self.find_element(RegistrationPageLocators.LOCATOR_SEND_FORM_REGISTRATION).click()
        time.sleep(7)

import pytest
from selenium import webdriver
import os


@pytest.fixture(scope="session")
def chromeBrowser():
    driver = webdriver.Chrome(executable_path=os.path.abspath(__file__).replace("\\", "/").replace("conftest.py", "").rstrip("/")
                              + "/webdrivers/chromedriver.exe")
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def operaBrowser():
    driver = webdriver.Opera(
        executable_path=os.path.abspath(__file__).replace("\\", "/").replace("conftest.py", "").rstrip(
            "/") + "/webdrivers/operadriver.exe")
    driver.maximize_window()
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def firefoxBrowser():
    driver = webdriver.Firefox(
        executable_path=os.path.abspath(__file__).replace("\\", "/").replace("conftest.py", "").rstrip(
            "/") + "/webdrivers/geckodriver.exe")
    driver.maximize_window()
    yield driver
    driver.quit()
